<?php

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Exemplo requisição get
Route::get('/teste',function(Request $request){
    return "ok";
});
*/

/* Exemplo requisição post
Route::post('/teste',function (Request $request) {
    return $request->all();
});
*/

//As funções de retorno serão implementadas diretamente aqui, porém
// é recomendado que sejam implementadas em controllers 
// para maior organização
Route::post('/cadastro',  [APIController::class, 'Cadastro']);
Route::post('/login',  [APIController::class, 'Login']);
Route::middleware('auth:api')->put('/perfil',  [APIController::class, 'Perfil']);