<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


class APIController extends Controller
{
    public function Cadastro(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200',
            'email' => 'required|string|email|max:200|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);


        if ($validator->fails()) {
            return $validator->errors();
        }

        
        $data = $request->all();
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'imagem'=> 'perfils/no-photo.png',
        ]);

        $user->imagem = asset($user->imagem);
        $user->token = $user->createToken($user->email)->accessToken; // método implementado por meio do use HasApiTokens
        return $user;
    }

    public function Login(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:200',
            'password' => 'required|string'
        ]);


        if ($validator->fails()) {
            return $validator->errors();
        }

        if (Auth::attempt([
            'email' => $data['email'],
            'password' => $data['password']
        ])) {
            $user = auth()->user();
            $user->imagem = asset($user->imagem);
            $user->token = $user->createToken($user->email)->accessToken; // método implementado por meio do use HasApiTokens
            return $user;
        } else {
            return 'false';
        }
    }

    public function Perfil(Request $request)
    {
        $data = $request->all();
        $user =  $request->user();

        if(isset($data['password'])){
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:200',
                'email' => ['required','string','email','max:200',Rule::unique('users')->ignore($user->id)],
                'password' => 'required|string|min:8|confirmed'
            ]);
    
    
            if ($validator->fails()) {
                return $validator->errors();
            }

            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:200',
                'email' => ['required','string','email','max:200',Rule::unique('users')->ignore($user->id)],
            ]);
    
    
            if ($validator->fails()) {
                return $validator->errors();
            }

            $user->name = $data['name'];
            $user->email = $data['email'];

        }
        //Pegando a extensão do base64
        $extension = substr($data['imagem'],11,strpos($data['imagem'],';')- 11);

        //Preparando o base64
        $file = str_replace('data:image/'.$extension.';base64','',$data['imagem']);
        $file = base64_decode($file);


        if(isset($data['imagem'])){
        $idphoto = uniqid();
        $path = 'perfils/';
        //$finalPath = $path.'/perfil_id'.$user->id;
        $extension = substr($data['imagem'],11,strpos($data['imagem'],';')- 11);
        $url = $path.$idphoto.'.'.$extension;
        
        //Validando o base64
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'svg', 'jpeg'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                [
                    '', '', '',
                ],
                $explode[0]
            );

            // check file format
            if (!in_array($format, $allow)) {
                return false;
            }

            // check base64 format
            if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                return false;
            }

            return true;
        });

        $validator = Validator::make($request->all(), [
            'imagem' => 'base64image',
        ],['base64image' => 'O arquivo não passou na validação']);


        if ($validator->fails()) {
            return $validator->errors();
        }

        //Terminando a validação do base64


        $file = str_replace('data:image/'.$extension.';base64,','',$data['imagem']);
        $file = base64_decode($file);

        if (!file_exists($path)) {
            mkdir($path,0700);
        }
        if (file_exists($user->imagem)) {
            unlink($user->imagem);
        }
        //Storage::put($url, $file);
        file_put_contents($url,$file);

        $user->imagem = $url;
        }
       
        $user->save();

        $user->imagem = asset($user->imagem);
        $user->token = $user->createToken($user->email)->accessToken; // método implementado por meio do use HasApiTokens
        return $request->user();
    }
}
